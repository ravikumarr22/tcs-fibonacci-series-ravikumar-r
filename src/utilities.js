function checkIfWeekend() {
  var today = new Date();
  /**
   * if getDay method has 6 or 0 can be treated as weekend and return true
   */
  if (today.getDay() === 6 || today.getDay() === 0) return true;
}

function checkIfPrimeOrCompositeNumber(number) {
  /**
   * count - if its divided only by 1 and itself is prime else Composite
   * number - number to be checked for Prime Or Composite
   *
   * prime Number eg : 1, 2, 3, 5, 7, 11 etc ( can be divided only by 1 and itself)
   * composite Number eg : 4, 6, 8, 10, 12, 14, 15 etc ( can be divided by 1, itself and other numbers )
   */
  let count = 0;
  for (let i = 1; i <= number; i++) {
    if (number % i === 0) count++;
  }
  /**
   * check if Today is weekend and show wic & woe else tik & toe
   */
  let checkIfTodayIsWeekend = checkIfWeekend();
  return count > 2
    ? checkIfTodayIsWeekend
      ? "woe"
      : "toe"
    : checkIfTodayIsWeekend
    ? "wic"
    : "tic";
}

function generateFibonacciSeries(number) {
  let data = [];
  var fib1 = 1;
  var fib2 = 0;
  for (let i = 1; i <= number; i++) {
    let fib3 = fib1 + fib2;
    /**
     * when fib3 is greater than number
     * eg: fib series for 25, it break loop when it get 21 since
     *  next fib3 will be greater than number else push the code to data
     */
    if (fib3 > number) {
      break;
    } else {
      /**
       * checkIfPrimeOrCompositeNumber function validates prime or composite and again checks
       * if its weekend using checkIfWeekend which has been used inside checkIfPrimeOrCompositeNumber
       * and return approriate values as an array
       */
      let checkPrimeOrCompositeNumberAndWeekend = checkIfPrimeOrCompositeNumber(
        fib3
      );
      data.push(checkPrimeOrCompositeNumberAndWeekend);
      fib1 = fib2;
      fib2 = fib3;
    }
  }
  return data;
}

export { generateFibonacciSeries };

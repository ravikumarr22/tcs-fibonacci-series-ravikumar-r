import { useState } from "react";

/**
 * Reference - https://dev.to/admantium/react-creating-a-custom-hook-for-pagination-jni
 */

function usePagination(data, itemsPerPage) {
  const [currentPage, setCurrentPage] = useState(1);
  const maxPage = Math.ceil(data.length / itemsPerPage);

  /**
   * currentData method splits data based on items per page
   */
  function currentData() {
    const begin = (currentPage - 1) * itemsPerPage;
    const end = begin + itemsPerPage;
    return data.slice(begin, end);
  }

  /**
   * next method used to navigate to next page and always use currentPage
   * for active class style
   */
  function next() {
    setCurrentPage(() => Math.min(currentPage + 1, maxPage));
  }

  /**
   * prev method used to navigate to previous page and always use currentPage
   * for active class style
   */
  function prev() {
    setCurrentPage(() => Math.max(currentPage - 1, 1));
  }

  /**
   * jump method has page parameter helps to navigate the particular page number
   * @param {*} page - page number to jump
   */
  function jump(page) {
    const pageNumber = Math.max(1, page);
    setCurrentPage(() => Math.min(pageNumber, maxPage));
  }

  return { next, prev, jump, currentData, currentPage, maxPage };
}

export default usePagination;

import React, { useState } from "react";
import { generateFibonacciSeries } from "./utilities";
import usePagination from "./Pagination";
import "./App.css";

function App() {
  const [inputValue, setInputValue] = useState("");
  const [validationError, setValidationError] = useState(false);
  const [data, setData] = useState([]);
  let [, setPageNum] = useState(1);

  const MAX_INPUT_VALUE = 2000;
  const PER_PAGE = 10;
  const totalPages = Math.ceil(data.length / PER_PAGE);
  const _DATA = usePagination(data, PER_PAGE);

  const changeInputValue = (e) => {
    e.target.value > MAX_INPUT_VALUE
      ? setValidationError(true)
      : setValidationError(false);
    setInputValue(e.target.value);
  };

  const generateFibonacci = (e, number) => {
    // prevent reload of the page after form submit since using form
    e.preventDefault();
    if (!number) {
      alert("Please enter a number");
      return false;
    }
    // return fib series like 'tic & toe' or 'wic & woe' format
    const result = generateFibonacciSeries(number);
    setData(result);
  };

  const handleChange = (e, page) => {
    setPageNum(page);
    _DATA.jump(page);
  };

  return (
    <div className="container">
      <h1>TCS Fibonacci Series by Ravikumar R</h1>
      <form
        className="fibonacci_form"
        data-testid="fibonacci_form"
        onSubmit={(e) => generateFibonacci(e, inputValue)}
      >
        <input
          type="text"
          className="text_input"
          data-testid="text_input"
          value={inputValue}
          onChange={(e) => changeInputValue(e)}
        />
        {validationError && (
          <p className="error" data-testid="error">
            Number should be positive and less than {MAX_INPUT_VALUE}
          </p>
        )}
        <br />
        <button
          className="button_submit"
          data-testid="button_submit"
          type="submit"
          disabled={validationError}
        >
          Genrate Fibonacci Series
        </button>
      </form>

      <ul className="list_container" data-testid="list_container">
        {_DATA.currentData().map((val, index) => {
          return (
            // className values are either wic/tic (blue) or woe/toe(green)
            <li key={index} className={val}>
              {val}
            </li>
          );
        })}
      </ul>

      {/* totalPages indicates total pages, renders only if the page count is more than 1 */}
      {totalPages > 1 && (
        <div className="pagination">
          <ul className="pagination_list" data-testid="pagination_list">
            <li
              className="prevButton"
              data-testid="prevButton"
              onClick={_DATA.prev}
            >
              {"<"}
            </li>

            {/* generating Array using totalPages (total page count like 3) and looping pages using ul li */}
            {Array.from(new Array(totalPages)).map((page, PageIndex) => {
              return (
                <li
                  key={PageIndex}
                  className={`pagination-button_${
                    PageIndex + 1 === _DATA.currentPage
                      ? "_active"
                      : "not_active"
                  }`}
                  data-testid={`pagination-button_${
                    PageIndex + 1 === _DATA.currentPage
                      ? "_active"
                      : "not_active"
                  }`}
                  onClick={(e) => handleChange(e, PageIndex + 1)}
                >
                  {PageIndex + 1}
                </li>
              );
            })}
            <li
              className="nextButton"
              data-testid="nextButton"
              onClick={_DATA.next}
            >
              {">"}
            </li>
          </ul>
        </div>
      )}
    </div>
  );
}

export default App;

import React from "react";
import { render, fireEvent } from "@testing-library/react";
import App from "./App";

describe("render fibonaaci series with all possible scenarios", function () {
  test("should have title in the page", () => {
    const { getByText } = render(<App />);
    const linkElement = getByText(/TCS Fibonacci Series by Ravikumar R/i);
    expect(linkElement).toBeInTheDocument();
  });

  test("should have form with className fibonacci_form ", () => {
    const { getByTestId } = render(<App />);
    let form = getByTestId("fibonacci_form");
    expect(form).toBeInTheDocument();
  });

  test("should have input field with className text_input ", () => {
    const { getByTestId } = render(<App />);
    let inputField = getByTestId("text_input");
    expect(inputField).toBeInTheDocument();
  });

  test("should have button with className button_submit", () => {
    const { getByTestId } = render(<App />);
    let button = getByTestId("button_submit");
    expect(button).toBeInTheDocument();
  });

  test("should have valid input value ", () => {
    const { getByTestId } = render(<App />);
    let input = getByTestId("text_input");
    fireEvent.change(input, { target: { value: "25" } });
    expect(input.value).toBe("25");
  });

  test("should have input validation error ", () => {
    const { getByTestId } = render(<App />);
    let input = getByTestId("text_input");
    fireEvent.change(input, { target: { value: "2002" } });

    let errorMessage = getByTestId("error");
    expect(errorMessage).toBeInTheDocument();
  });

  test("should alert when input value is empty and click submit", () => {
    jest.spyOn(window, "alert").mockImplementation(() => {});

    const { getByTestId } = render(<App />);
    let button = getByTestId("button_submit");

    // no input set and clicking submit button / enter
    fireEvent.click(button);
    expect(window.alert).toBeCalledWith("Please enter a number");
  });

  test("should have rendered 8 fibonacci list forinput 25 ", () => {
    const { getByTestId } = render(<App />);
    let input = getByTestId("text_input");
    let button = getByTestId("button_submit");

    /**
     * setting input value 25 and then clicking button submit
     * that should generate 8 li elements under ul has className `list_container`
     */
    fireEvent.change(input, { target: { value: "25" } });
    fireEvent.click(button);

    let fibonacci_List = getByTestId("list_container");

    // fibonacci Number 25 generates 8 numbers
    expect(fibonacci_List.children.length).toBe(8);
  });

  test("should have pagination with className pagination_list in the page", () => {
    const { getByTestId } = render(<App />);
    let input = getByTestId("text_input");
    let button = getByTestId("button_submit");

    /**
     * setting input value 2000 (MAX_INPUT_VALUE)
     * and clicking submit button / press enter
     */
    fireEvent.change(input, { target: { value: "2000" } });
    fireEvent.click(button);

    /**
     * get pagination by data-testid validates if exist in the document
     */
    let pagination_List = getByTestId("pagination_list");

    /**
     * pagination_List (ul) shoud be present in the document
     */
    expect(pagination_List).toBeInTheDocument();
  });

  test("should have next, prev buttons when input value is 2000", () => {
    const { getByTestId } = render(<App />);
    let input = getByTestId("text_input");
    let button = getByTestId("button_submit");

    /**
     * setting input value 2000 (MAX_INPUT_VALUE)
     * and clicking submit button / press enter
     */
    fireEvent.change(input, { target: { value: "2000" } });
    fireEvent.click(button);

    /**
     * This renders total 17 items on the page, 10 in pagination 1 and 7 in pagination 2
     */

    let pagination_List = getByTestId("pagination_list");

    /**
     * pagination_List children (li) generates [prev, 1, 2, next] when input value is 2000
     */
    expect(pagination_List.children.length).toBe(4);
  });

  test("should prev and next pagination buttons", () => {
    const { getByTestId } = render(<App />);
    let input = getByTestId("text_input");
    let button = getByTestId("button_submit");

    /**
     * setting input value 2000 (MAX_INPUT_VALUE)
     * and clicking submit button / press enter
     */
    fireEvent.change(input, { target: { value: "2000" } });
    fireEvent.click(button);

    /**
     * This renders total 17 items on the page, 10 in pagination 1 and 7 in pagination 2
     */

    let fibonacci_List = getByTestId("list_container");
    let pagination_List = getByTestId("pagination_list");

    /**
     * input 2000 generates 17 number , 10 in first page and 7 in second page
     * also generates 4 pagination button [prev, 1, 2, next]
     */

    /**
     * checking next button (index 2) and list count shoud be 7
     */
    fireEvent.click(pagination_List.children[2]);
    expect(fibonacci_List.children.length).toBe(7);

    /**
     * checking prev button (index 1) and list count shoud be 10
     */
    fireEvent.click(pagination_List.children[1]);
    expect(fibonacci_List.children.length).toBe(10);
  });
});

describe("should test is weekend by spyOn getDay method in the Date Object constructor", function () {
  beforeEach(() => {
    // 6 0r 0 falls under weekend
    jest.spyOn(Date.prototype, "getDay").mockReturnValue(6);
    jest
      .spyOn(Date.prototype, "toISOString")
      .mockReturnValue("2020-09-027T00:00:00.000Z");
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  test("should render wic fo weekend", () => {
    const { container, getByTestId } = render(<App />);
    let input = getByTestId("text_input");
    let button = getByTestId("button_submit");
    fireEvent.change(input, { target: { value: "25" } });
    fireEvent.click(button);

    let fibonacci_List = getByTestId("list_container");

    // fibonacci Number 25 generates 8 numbers
    expect(fibonacci_List.children.length).toBe(8);
    const containsWicClass = container
      .getElementsByClassName("list_container")[0]
      .firstChild.classList.contains("wic");
    expect(containsWicClass).toBe(true);
  });
});

describe("should not weekend by spyOn getDay method in the Date Object constructor", function () {
  beforeEach(() => {
    // 1,2,3,4,5 falls under week days
    jest.spyOn(Date.prototype, "getDay").mockReturnValue(2);
    jest
      .spyOn(Date.prototype, "toISOString")
      .mockReturnValue("2020-09-027T00:00:00.000Z");
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  test("should render tic for week days", () => {
    const { container, getByTestId } = render(<App />);
    let input = getByTestId("text_input");
    let button = getByTestId("button_submit");
    fireEvent.change(input, { target: { value: "25" } });
    fireEvent.click(button);

    let fibonacci_List = getByTestId("list_container");

    // fibonacci Number 25 generates 8 numbers
    expect(fibonacci_List.children.length).toBe(8);
    const containsWicClass = container
      .getElementsByClassName("list_container")[0]
      .firstChild.classList.contains("tic");
    expect(containsWicClass).toBe(true);
  });
});
